﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FunnyGamePet : MonoBehaviour
{
    RectTransform rt;

    public GameObject Root_Game;

    public Image Img_Seed;
    BoxCollider2D phy;

    Animator ani;

    public Sprite CatchSprite;

    public Text Text_CoinNum;


    Vector2 DefaultRatPos;
    Vector2 DefaultRatSize;
    Vector2 RatCatchSize = new Vector2( 170.4f, 271.8f );

    public static int CoinNum = 0;
    public static int SeedNum = 10;

    public Image Img_Success;
    public Sprite Success;
    public Sprite Fail;

	public AudioGameCtrl audioGame;

    // Use this for initialization
    void Start()
    {
        CoinNum = 0;
        SeedNum = 10;
        phy = GetComponent<BoxCollider2D>();
        ani = GetComponent<Animator>();
        rt = transform as RectTransform;
        DefaultRatPos = rt.anchoredPosition;
        DefaultRatSize = new Vector2( rt.sizeDelta.x, rt.sizeDelta.y );
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnCollisionEnter2D( Collision2D collision )
    {
		audioGame.ASHit.Play ();
        collision.gameObject.name.Equals( "Img_Seed" );
        Img_Seed.gameObject.SetActive(false);
        //RestPos();
        ani.enabled = false;
        GetComponent<Image>().sprite = CatchSprite;
        rt.sizeDelta = RatCatchSize;
        rt.anchoredPosition = new Vector2( rt.anchoredPosition.x, 412.6f );
        CoinNum++;
        Text_CoinNum.text = string.Format( "{0}/10", CoinNum );
        Invoke( "StartMove", 1f );
        if ( RestGame.isSuccess )
            PlayerPrefs.SetInt( GameInit.COINKEY, PlayerPrefs.GetInt( GameInit.COINKEY ) + 1 );
    }

    void StartMove()
    {
        RestPos();
        Img_Seed.gameObject.SetActive(true);
        ChangeForce.ResetGame();
        if ( CoinNum >= 8 && SeedNum == 0 && !RestGame.isSuccess )
        {
            Img_Success.gameObject.SetActive( true );
            Img_Success.sprite = Success;
            Root_Game.SetActive( false );
            RestGame.isSuccess = true;
            PlayerPrefs.SetInt( GameInit.COINKEY, PlayerPrefs.GetInt( GameInit.COINKEY ) + CoinNum );
			audioGame.ASVic.Play ();
        }

        ani.enabled = true;
        rt.sizeDelta = DefaultRatSize;
        rt.anchoredPosition = DefaultRatPos;
    }

    public void RestPos()
    {
        Img_Seed.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        Img_Seed.transform.localPosition = new Vector3( 0, -24.48f, 0 );
        Img_Seed.transform.localRotation = Quaternion.identity;
    }
}

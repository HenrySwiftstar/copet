﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RestGame : MonoBehaviour {

    public Text Text_SeedNum;
    public Text Text_CoinNum;

    public Text Text_BonusHelp;
    public Text Text_BonusTime;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void BonusGameClick()
    {
        ChangeForce.ResetGame();
        Text_BonusHelp.gameObject.SetActive( true );
        ChangeForce.isBonusShow = true;
        gameObject.SetActive( true );
        Invoke( "CloseBonusHelp", 3f );
    }

    void CloseBonusHelp()
    {
        Text_BonusHelp.gameObject.SetActive( false );
        Text_BonusTime.gameObject.SetActive( true );
        ChangeForce.isBonusShow = false;
        ChangeForce.bonusTime = 10f;
        Text_BonusTime.text = string.Format( "Time : {0} s", ( int )ChangeForce.bonusTime );
        InvokeRepeating( "RepeatDownBonusTime", 1f, 1f );
    }

    void RepeatDownBonusTime()
    {
        ChangeForce.bonusTime = Mathf.Clamp( ChangeForce.bonusTime - 1, 0, 10 );
        Text_BonusTime.text = string.Format( "Time : {0} s", ( int )ChangeForce.bonusTime );
        if ( ChangeForce.bonusTime == 0 )
            CancelInvoke();
    }

    public static bool isSuccess = false;

    public void RestGameClick()
    {
        Text_BonusHelp.gameObject.SetActive( false );
        Text_BonusTime.gameObject.SetActive( false );
        
        if ( isSuccess )
        {
            BonusGameClick();
            return;
        }

        isSuccess = false;
        ChangeForce.ResetGame();
        FunnyGamePet.CoinNum = 0;
        Text_CoinNum.text = string.Format( "{0}/10", FunnyGamePet.CoinNum );
        FunnyGamePet.SeedNum = 10;
        Text_SeedNum.text = string.Format( "{0}/10", FunnyGamePet.SeedNum );
        gameObject.SetActive( true );
    }
}

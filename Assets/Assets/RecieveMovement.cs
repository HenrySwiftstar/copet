﻿using UnityEngine;
using System.Collections;

public class RecieveMovement : MonoBehaviour {


    Vector3 newposition;
    public float speed;
    public float walkRange;
    public GameObject graphices;
	// Use this for initialization
	void Start () {
        newposition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	if(Vector3.Distance (newposition, this.transform.position) > walkRange){
            this.transform.position = Vector3.MoveTowards(this.transform.position, newposition, speed * Time.deltaTime);
            Quaternion transRot = Quaternion.LookRotation(newposition - this.transform.position, Vector3.up);
            graphices.transform.rotation = Quaternion.Slerp(transRot, graphices.transform.rotation, 0.7f);
        }
	}
    
    public void RecievedMove(Vector3 movePos)
    {
        newposition = movePos;
    }
}

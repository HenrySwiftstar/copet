﻿using UnityEngine;
using System.Collections;
using System;

public class LeftClick : MonoBehaviour
{
    public Animator anim;
    private Touch touch;

    public TapMoveRat tapRat;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        tapRat = GetComponent<TapMoveRat>();

    }

    // Update is called once per frame
    void Update()
    {
        if ( tapRat.Target.x - tapRat.transform.position.x > 0 )
        {
            anim.SetBool( "RIGHT", true );
            anim.SetBool( "LEFT", false );
        }
        else if ( tapRat.Target.x - tapRat.transform.position.x < 0 )
        {
            anim.SetBool( "LEFT", true );
            anim.SetBool( "RIGHT", false );
        }
        else
        {
            anim.SetBool( "LEFT", false );
            anim.SetBool( "RIGHT", false );
        }

        //var mouse = new Vector2( Input.mousePosition.x, Screen.height - Input.mousePosition.y );
        //if ( mouse.x < Screen.width / 2 )
        //{
        //    if ( Input.GetMouseButtonDown( 0 ) )
        //    {

        //        anim.SetBool( "LEFT", true );

        //    }
        //    else if ( Input.GetMouseButtonUp( 0 ) )
        //    {
        //        anim.SetBool( "LEFT", false );
        //    }
        //}
        //if ( mouse.x > Screen.width / 2 )
        //{
        //    if ( Input.GetMouseButtonDown( 0 ) )
        //    {
        //        anim.SetBool( "RIGHT", true );
        //    }
        //    else if ( Input.GetMouseButtonUp( 0 ) )
        //    {
        //        anim.SetBool( "RIGHT", false );
        //    }

        //}
    }
}

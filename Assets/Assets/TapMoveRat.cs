﻿using UnityEngine;
using System.Collections;

public class TapMoveRat : MonoBehaviour
{
    bool flag = false;
    public GameObject Player;
    public Animator anim;
    public bool LEFT;
    public bool RIGHT;
    public float speed = 0.5f;
    private Vector3 target;
    public Vector3 Target { get { return target; } }

    public bool isTap = false;

    void Start()
    {
        Player = gameObject;
        anim = Player.GetComponent<Animator>();
        target = transform.position;
        InvokeRepeating( "RanomWalk", 2f, 2f );

    }

    void RanomWalk()
    {
        Vector3 testView = Vector3.zero;
        Vector3 testTarge = transform.position;
        do
        {
            testTarge = transform.position + new Vector3(
                  Random.Range( -100, 100 ),
                  Random.Range( -100, 100 ),
                  0 );
            testView = Camera.main.WorldToScreenPoint( testTarge );
        } while ( testView.x < 30f || testView.y < 15f || testView.x > 650f || testView.y > 950f );

        target = testTarge;
        Debug.Log( testView );
    }

    bool isOver = false;
    public void OnMouseDrag()
    {
        CancelInvoke();
        isTap = false;
        transform.position = Camera.main.ScreenToWorldPoint( Input.mousePosition );
        transform.position = new Vector3( transform.position.x, transform.position.y, 0f );
        target = transform.position;
    }

    public void OnMouseEnter()
    {

        isOver = true;
    }

    public void OnMouseOver()
    {
        isOver = true;
    }

    public void OnMouseExit()
    {
        isOver = false;
        if ( !IsInvoking( "RanomWalk" ) )
            InvokeRepeating( "RanomWalk", 2f, 2f );
    }

    public void MoveRatToInputPoint()
    {
        if ( !isOver )
        {

            target = Camera.main.ScreenToWorldPoint( Input.mousePosition );
            target.z = transform.position.z;
            isTap = true;
            CancelInvoke();
        }
    }

    void Update()
    {

        if (!anim.GetCurrentAnimatorStateInfo(0).IsTag("Eat"))
            transform.position = Vector3.MoveTowards( transform.position, target, speed * 3 * Time.deltaTime );
        if ( transform.position == target && isTap )
        {
            InvokeRepeating( "RanomWalk", 2f, 2f );
            isTap = false;
        }


    }


}









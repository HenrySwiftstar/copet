﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LvCtrl : MonoBehaviour {

    public Text Text_LV;
    public Text Text_EXP;
    public Slider Slider_EXP;

	// Use this for initialization
	void Start () {
        RefreshLVEXP();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void RefreshLVEXP()
    {
        Text_LV.text = PlayerPrefs.GetInt( GameInit.LVKEY ).ToString();


        int exp = PlayerPrefs.GetInt( GameInit.EXPKEY );
        int maxExp = PlayerPrefs.GetInt( GameInit.LVKEY ) * 10;
        Text_EXP.text = string.Format( "{0} / {1}", exp, maxExp ).ToString();
        Slider_EXP.minValue = 0;
        Slider_EXP.maxValue = maxExp;
        Slider_EXP.value = exp;
    }

    public void GetExp(int exp)
    {
        int nowExp = PlayerPrefs.GetInt( GameInit.EXPKEY );
        int nowLv = PlayerPrefs.GetInt( GameInit.LVKEY );
        int nowMaxExp = PlayerPrefs.GetInt( GameInit.LVKEY ) * 10;

        for (int i = exp ; i > 0 ; i-- )
        {
            nowExp += 1;
            if ( nowExp == nowMaxExp )
            {
                nowLv = Mathf.Min( 25, nowLv + 1 );
                nowExp = 0;
                nowMaxExp = nowLv*10;
            }
        }

        PlayerPrefs.SetInt( GameInit.EXPKEY, nowExp );
        PlayerPrefs.SetInt( GameInit.LVKEY, nowLv );

        RefreshLVEXP();
    }
}

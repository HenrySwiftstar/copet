﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StoreCtrl : MonoBehaviour {

    enum ItemType : int
    {       
        None = 0, 
        Food1 = 1,
        Food2 = 2,
        Food3 = 3,
        cup = 4,
        wheel = 5,
		Background =6
    }

    public Text Text_Coin;

    public GameObject Pnl_Check;
    public GameObject Pnl_NoCoin;

    public GameInit gameInit;

    int nowWheelType = 0;
	int nowBackgroundType = 0;
	int nowDecoType = 0;
    int nowOpenType = ( int )ItemType.None;

    public void Open()
    {
        RefreshCoin();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void RefreshCoin()
    {
        Text_Coin.text = PlayerPrefs.GetInt( GameInit.COINKEY ).ToString();
    }

    public void ClickWheel(int type)
    {
        switch (type )
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
        }

        nowWheelType = type;
    }

	public void ClickDeco(int type)
	{
		nowDecoType = type;
	}

	public void ClickBackground(int type)
	{
		nowBackgroundType = type;
	}

    public void ClickItem(int type)
    {
        int coinNum = PlayerPrefs.GetInt( GameInit.COINKEY );
        switch ( type )
        {
            case ( int )ItemType.Food1:
                if ( coinNum < 5 )
                    Pnl_NoCoin.SetActive( true );
                break;
            case ( int )ItemType.Food2:
                if ( coinNum < 8 )
                    Pnl_NoCoin.SetActive( true );
                break;
            case ( int )ItemType.Food3:
                if ( coinNum < 15 )
                    Pnl_NoCoin.SetActive( true );
                break;
            case ( int )ItemType.cup:
                if ( coinNum < 50 )
                    Pnl_NoCoin.SetActive( true );
                break;
            case ( int )ItemType.wheel:
                if ( coinNum < 100 )
                    Pnl_NoCoin.SetActive( true );
                break;
			case (int ) ItemType.Background:
				if (coinNum < 500)
					Pnl_NoCoin.SetActive (true);
				break;
        }

        if ( Pnl_NoCoin.gameObject.activeSelf )
            return;
        nowOpenType = type;
        Pnl_Check.SetActive( true );
    }

    public void BuyOk()
    {
        int coinNum = PlayerPrefs.GetInt( GameInit.COINKEY );
        switch ( nowOpenType )
        {
            case ( int )ItemType.Food1:
                PlayerPrefs.SetInt( GameInit.COINKEY, Mathf.Max( 0, coinNum - 5 ) );
                PlayerPrefs.SetInt( GameInit.FOOD1_NUM,
                    PlayerPrefs.GetInt( GameInit.FOOD1_NUM ) + 1 );
                break;
            case ( int )ItemType.Food2:
                PlayerPrefs.SetInt( GameInit.COINKEY, Mathf.Max( 0, coinNum - 8 ) );
                PlayerPrefs.SetInt( GameInit.FOOD2_NUM,
                  PlayerPrefs.GetInt( GameInit.FOOD2_NUM ) + 1 );
                break;
            case ( int )ItemType.Food3:
                PlayerPrefs.SetInt( GameInit.COINKEY, Mathf.Max( 0, coinNum - 15 ) );
                PlayerPrefs.SetInt( GameInit.FOOD3_NUM,
                  PlayerPrefs.GetInt( GameInit.FOOD3_NUM ) + 1 );
                break;
            case ( int )ItemType.cup:
                PlayerPrefs.SetInt( GameInit.COINKEY, Mathf.Max( 0, coinNum - 50 ) );
                PlayerPrefs.SetInt( GameInit.CUPKEY, 1 );
                break;
            case ( int )ItemType.wheel:
                PlayerPrefs.SetInt( GameInit.COINKEY, Mathf.Max( 0, coinNum - 100 ) );
                PlayerPrefs.SetInt( GameInit.WHEELKEY, 1 );
                break;
			case ( int )ItemType.Background:
				PlayerPrefs.SetInt( GameInit.COINKEY, Mathf.Max( 0, coinNum - 500 ) );
				break;
        }

        PlayerPrefs.SetInt( GameInit.WHEELTYPE, nowWheelType );
		PlayerPrefs.SetInt (GameInit.DECOTYPE, nowDecoType);
		PlayerPrefs.SetInt (GameInit.BACKGROUNDTYPE, nowBackgroundType);

        RefreshCoin();
        gameInit.RefreshDecoration();
    }
}

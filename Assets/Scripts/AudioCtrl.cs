﻿using UnityEngine;
using System.Collections;

public class AudioCtrl : MonoBehaviour {

	public AudioSource UI;
	public AudioSource BGM;
	public AudioSource Eat;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Enable(bool enable)
	{
		UI.mute = !enable;
		BGM.mute = !enable;
		Eat.mute = !enable;
		PlayerPrefs.SetInt (GameInit.AudioTog, enable ? 1 : 0);
	}
}

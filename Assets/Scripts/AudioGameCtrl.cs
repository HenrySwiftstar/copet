﻿using UnityEngine;
using System.Collections;

public class AudioGameCtrl : MonoBehaviour {

	public AudioSource ASBGM;
	public AudioSource ASHit;
	public AudioSource ASVic;
	public AudioSource ASFail;

	// Use this for initialization
	void Start () {
		Enable(PlayerPrefs.GetInt(GameInit.AudioTog)==1?true:false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Enable(bool enable)
	{
		ASBGM.mute = !enable;
		ASHit.mute = !enable;
		ASVic.mute = !enable;
		ASFail.mute = !enable;
	}
}

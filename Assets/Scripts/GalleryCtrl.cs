﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GalleryCtrl : MonoBehaviour {

    public GameInit gameInit;
    public Text[] Text_Enable;
    public Toggle[] Tog_Rat;

    public Image[] Img_Rat;

    const string ENABLESTR = "已解鎖";
    const string DISABLESTR = "未解鎖";

    // Use this for initialization
    void Start () {

        int lv = PlayerPrefs.GetInt( GameInit.LVKEY );
        for (int i = 0 ; i < Text_Enable.Length ; i++ )
        {
            int tempLv = 5;
            if ( lv >= tempLv * (i + 1) )
            {
                Text_Enable[i].text = ENABLESTR;
                Tog_Rat[i].gameObject.SetActive( true );
                Img_Rat[i].color = Color.white;
            }           
            else
            {
                Text_Enable[i].text = DISABLESTR;
                Tog_Rat[i].gameObject.SetActive( false );
                Img_Rat[i].color = Color.black;
            }              
        }

        Tog_Rat[0].isOn = PlayerPrefs.GetInt( GameInit.RAT1 ) == 0 ? false : true;
        Tog_Rat[1].isOn = PlayerPrefs.GetInt( GameInit.RAT2 ) == 0 ? false : true;
        Tog_Rat[2].isOn = PlayerPrefs.GetInt( GameInit.RAT3 ) == 0 ? false : true;
        Tog_Rat[3].isOn = PlayerPrefs.GetInt( GameInit.RAT4 ) == 0 ? false : true;
        Tog_Rat[4].isOn = PlayerPrefs.GetInt( GameInit.RAT5 ) == 0 ? false : true;

        Tog_Rat[0].onValueChanged.AddListener( Tog_Enable1 );
        Tog_Rat[1].onValueChanged.AddListener( Tog_Enable2 );
        Tog_Rat[2].onValueChanged.AddListener( Tog_Enable3 );
        Tog_Rat[3].onValueChanged.AddListener( Tog_Enable4 );
        Tog_Rat[4].onValueChanged.AddListener( Tog_Enable5 );
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Tog_Enable1(bool enable)
    {
        PlayerPrefs.SetInt( GameInit.RAT1, enable ? 1 : 0 );
        gameInit.EnableRefreshRat();
    }

    public void Tog_Enable2( bool enable )
    {
        PlayerPrefs.SetInt( GameInit.RAT2, enable ? 1 : 0 );
        gameInit.EnableRefreshRat();
    }

    public void Tog_Enable3( bool enable )
    {
        PlayerPrefs.SetInt( GameInit.RAT3, enable ? 1 : 0 );
        gameInit.EnableRefreshRat();
    }

    public void Tog_Enable4( bool enable )
    {
        PlayerPrefs.SetInt( GameInit.RAT4, enable ? 1 : 0 );
        gameInit.EnableRefreshRat();
    }

    public void Tog_Enable5( bool enable )
    {
        PlayerPrefs.SetInt( GameInit.RAT5, enable ? 1 : 0 );
        gameInit.EnableRefreshRat();
    }
}

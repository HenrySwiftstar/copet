﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HungryCtrl : MonoBehaviour {

    public Text Text_Hungry;
    public Slider Slider_Hungry;
    public Image Img_Time;

    const int maxTime = 10;
    float nowTime = maxTime;

	// Use this for initialization
	void Start () {
        Img_Time.fillAmount = 1f;
        Text_Hungry.text = string.Format( "{0}/100", PlayerPrefs.GetInt( GameInit.HUNGRYKEY ) );
        Slider_Hungry.value = PlayerPrefs.GetInt( GameInit.HUNGRYKEY );
    }

    public void RefreshHungry()
    {
        Text_Hungry.text = string.Format( "{0}/100", PlayerPrefs.GetInt( GameInit.HUNGRYKEY ) );
        Slider_Hungry.value = PlayerPrefs.GetInt( GameInit.HUNGRYKEY );
    }
	
	// Update is called once per frame
	void Update () {
        nowTime =  Mathf.Max( 0, nowTime - Time.deltaTime );
        Img_Time.fillAmount = nowTime / maxTime;

        if ( nowTime == 0f )
        {
            Img_Time.fillAmount = 1f;
            int hungryNum = Mathf.Max( 0, PlayerPrefs.GetInt( GameInit.HUNGRYKEY ) - 1 );
            PlayerPrefs.SetInt( GameInit.HUNGRYKEY, hungryNum );
            Text_Hungry.text = string.Format( "{0}/100", PlayerPrefs.GetInt( GameInit.HUNGRYKEY ) );
            nowTime = maxTime;
            Slider_Hungry.value = PlayerPrefs.GetInt( GameInit.HUNGRYKEY );
        }
	}
}

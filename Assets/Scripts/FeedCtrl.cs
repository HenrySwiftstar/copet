﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FeedCtrl : MonoBehaviour {

    public Text Text_Food1;
    public Text Text_Food2;
    public Text Text_Food3;

    public Animator[] petAni;

    public LvCtrl lvCtrl;
    public HungryCtrl hungryCtrl;
	public AudioCtrl audioCtrl;

    public void Open()
    {
        Text_Food1.text = string.Format( "x {0}", PlayerPrefs.GetInt( GameInit.FOOD1_NUM ) );
        Text_Food2.text = string.Format( "x {0}", PlayerPrefs.GetInt( GameInit.FOOD2_NUM ) );
        Text_Food3.text = string.Format( "x {0}", PlayerPrefs.GetInt( GameInit.FOOD3_NUM ) );
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Feed(int type)
    {
        int foodNum = 0;
        int getExp = 0;
        switch(type)
        {
            case 1:
                if ( PlayerPrefs.GetInt( GameInit.FOOD1_NUM ) < 1 )
                    return;
                foodNum = Mathf.Max( 0, PlayerPrefs.GetInt( GameInit.FOOD1_NUM ) - 1 );
                PlayerPrefs.SetInt( GameInit.FOOD1_NUM, foodNum );
                PlayerPrefs.SetInt( GameInit.HUNGRYKEY, Mathf.Min( 100, PlayerPrefs.GetInt( GameInit.HUNGRYKEY ) + 10 ) );
                getExp = 4;
                break;
            case 2:
                if ( PlayerPrefs.GetInt( GameInit.FOOD2_NUM ) < 1 )
                    return;
                foodNum = Mathf.Max( 0, PlayerPrefs.GetInt( GameInit.FOOD2_NUM ) - 1 );
                PlayerPrefs.SetInt( GameInit.FOOD2_NUM, foodNum );
                PlayerPrefs.SetInt( GameInit.HUNGRYKEY, Mathf.Min( 100, PlayerPrefs.GetInt( GameInit.HUNGRYKEY ) + 20 ) );
                getExp = 8;
                break;
            case 3:
                if ( PlayerPrefs.GetInt( GameInit.FOOD3_NUM ) < 1 )
                    return;
                foodNum = Mathf.Max( 0, PlayerPrefs.GetInt( GameInit.FOOD3_NUM ) - 1 );
                PlayerPrefs.SetInt( GameInit.FOOD3_NUM, foodNum );
                PlayerPrefs.SetInt( GameInit.HUNGRYKEY, Mathf.Min( 100, PlayerPrefs.GetInt( GameInit.HUNGRYKEY ) + 50 ) );
                getExp = 20;
                break;
        }
		audioCtrl.Eat.Play ();
        PlayerPrefs.Save();
        lvCtrl.GetExp( getExp );
        hungryCtrl.RefreshHungry();

        for ( int i = 0 ; i < petAni.Length ; i++ )
            petAni[i].SetTrigger( "Eat" );
    }
}

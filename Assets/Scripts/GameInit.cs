﻿using UnityEngine;
using System.Collections;

public class GameInit : MonoBehaviour {


    public const string LVKEY = "LV";
    public const string EXPKEY = "EXP";
    public const string FOOD1_NUM = "FOOD1NUM";
    public const string FOOD2_NUM = "FOOD2NUM";
    public const string FOOD3_NUM = "FOOD3NUM";
    public const string CUPKEY = "CUP";
    public const string WHEELKEY = "WHEEL";

    public const string WHEELTYPE = "WHEELTYPE";
	public const string BACKGROUNDTYPE = "BACKTYPE";
	public const string DECOTYPE = "DECOTYPE";

    public const string COINKEY = "COIN";

    public const string HUNGRYKEY = "HUNGRY";

    public const string RAT1 = "RAT1";
    public const string RAT2 = "RAT2";
    public const string RAT3 = "RAT3";
    public const string RAT4 = "RAT4";
    public const string RAT5 = "RAT5";

	public const string AudioTog = "AudioTogKey";

    public GameObject[] Rats;

    public GameObject Cup;
    public GameObject Wheel;

    public Sprite[] WheelSprite;
	public Sprite[] BackSprite;
	public Sprite[] CupSprite;

	public UnityEngine.UI.Image ImgBack;

    void Awake()
    {
        if ( !PlayerPrefs.HasKey( LVKEY ) )
            PlayerPrefs.SetInt( LVKEY, 1 );
        if ( !PlayerPrefs.HasKey( EXPKEY ) )
            PlayerPrefs.SetInt( EXPKEY, 0 );
        if ( !PlayerPrefs.HasKey( FOOD1_NUM ) )
            PlayerPrefs.SetInt( FOOD1_NUM, 0 );
        if ( !PlayerPrefs.HasKey( FOOD2_NUM ) )
            PlayerPrefs.SetInt( FOOD2_NUM, 0 );
        if ( !PlayerPrefs.HasKey( FOOD3_NUM ) )
            PlayerPrefs.SetInt( FOOD3_NUM, 0 );
        if ( !PlayerPrefs.HasKey( CUPKEY ) )
            PlayerPrefs.SetInt( CUPKEY, 0 );
        if ( !PlayerPrefs.HasKey( WHEELKEY ) )
            PlayerPrefs.SetInt( WHEELKEY, 0 );

        if ( !PlayerPrefs.HasKey( WHEELTYPE ) )
            PlayerPrefs.SetInt( WHEELTYPE, 0 );
		if ( !PlayerPrefs.HasKey( BACKGROUNDTYPE ) )
			PlayerPrefs.SetInt( BACKGROUNDTYPE, 0 );
		if ( !PlayerPrefs.HasKey( DECOTYPE ) )
			PlayerPrefs.SetInt( DECOTYPE, 0 );

        if ( !PlayerPrefs.HasKey( COINKEY ) )
            PlayerPrefs.SetInt( COINKEY, 0 );

        if ( !PlayerPrefs.HasKey( HUNGRYKEY ) )
            PlayerPrefs.SetInt( HUNGRYKEY, 100 );

        if ( !PlayerPrefs.HasKey( RAT1 ) )
            PlayerPrefs.SetInt( RAT1, 0 );
        if ( !PlayerPrefs.HasKey( RAT2 ) )
            PlayerPrefs.SetInt( RAT2, 0 );
        if ( !PlayerPrefs.HasKey( RAT3 ) )
            PlayerPrefs.SetInt( RAT3, 0 );
        if ( !PlayerPrefs.HasKey( RAT4 ) )
            PlayerPrefs.SetInt( RAT4, 0 );
        if ( !PlayerPrefs.HasKey( RAT5 ) )
            PlayerPrefs.SetInt( RAT5, 0 );

		if ( !PlayerPrefs.HasKey( AudioTog ) )
			PlayerPrefs.SetInt( AudioTog, 1 );
    }

    public void GameReset()
    {
        PlayerPrefs.SetInt( LVKEY, 1 );
        PlayerPrefs.SetInt( EXPKEY, 0 );
        PlayerPrefs.SetInt( FOOD1_NUM, 0 );
        PlayerPrefs.SetInt( FOOD2_NUM, 0 );
        PlayerPrefs.SetInt( FOOD3_NUM, 0 );
        PlayerPrefs.SetInt( CUPKEY, 0 );
        PlayerPrefs.SetInt( WHEELKEY, 0 );

        PlayerPrefs.SetInt( WHEELTYPE, 0 );
		PlayerPrefs.SetInt( BACKGROUNDTYPE, 0 );
		PlayerPrefs.SetInt( DECOTYPE, 0 );

        PlayerPrefs.SetInt( COINKEY, 0 );

        PlayerPrefs.SetInt( HUNGRYKEY, 100 );

        PlayerPrefs.SetInt( RAT1, 0 );
        PlayerPrefs.SetInt( RAT2, 0 );
        PlayerPrefs.SetInt( RAT3, 0 );
        PlayerPrefs.SetInt( RAT4, 0 );
        PlayerPrefs.SetInt( RAT5, 0 );
	
		PlayerPrefs.SetInt( AudioTog, 1 );

        UnityEngine.SceneManagement.SceneManager.LoadScene( "intro" );
    }

    public void GoToIntro()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene( "intro" );
    }

    // Use this for initialization
    void Start()
    {

        Rats[0].SetActive( (PlayerPrefs.GetInt( RAT1 ) == 1) );
        Rats[1].SetActive( (PlayerPrefs.GetInt( RAT2 ) == 1) );
        Rats[2].SetActive( (PlayerPrefs.GetInt( RAT3 ) == 1) );
        Rats[3].SetActive( (PlayerPrefs.GetInt( RAT4 ) == 1) );
        Rats[4].SetActive( (PlayerPrefs.GetInt( RAT5 ) == 1) );

        RefreshDecoration();
    }
	
    public void EnableRefreshRat()
    {
        Rats[0].SetActive( (PlayerPrefs.GetInt( RAT1 ) == 1) );
        Rats[1].SetActive( (PlayerPrefs.GetInt( RAT2 ) == 1) );
        Rats[2].SetActive( (PlayerPrefs.GetInt( RAT3 ) == 1) );
        Rats[3].SetActive( (PlayerPrefs.GetInt( RAT4 ) == 1) );
        Rats[4].SetActive( (PlayerPrefs.GetInt( RAT5 ) == 1) );
    }

    public void RefreshDecoration()
    {
        Cup.SetActive( PlayerPrefs.GetInt( CUPKEY ) == 1 ? true: false );
		Cup.GetComponent<UnityEngine.UI.Image>().sprite = CupSprite[PlayerPrefs.GetInt( DECOTYPE )];
        Wheel.SetActive( PlayerPrefs.GetInt( WHEELKEY ) == 1 ? true : false );
        Wheel.GetComponent<UnityEngine.UI.Image>().sprite = WheelSprite[PlayerPrefs.GetInt( WHEELTYPE )];
		ImgBack.sprite = BackSprite[PlayerPrefs.GetInt( BACKGROUNDTYPE )];
    }

	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChangeForce : MonoBehaviour {

    public enum Direction
    {
        Right,
        Left
    }

    public GameObject Root_Game;

    public Slider slider;

    Direction dir = Direction.Right;

    public Rigidbody2D rigSeed;
    public ConstantForce2D seedPhy;

    public Text Text_SeedNum;
    static float throwTime = -1f;

    public Image Img_Success;
    public Sprite Success;
    public Sprite Fail;

    static bool isFly = false;
    public static bool isBonusShow = false;

    public static float bonusTime = 10f;

	public AudioGameCtrl audioGame;

	// Use this for initialization
	void Start () {
	
	}

    public static void ResetGame()
    {
        throwTime = -1f;
        isFly = false;
    }
	
	// Update is called once per frame
	void Update () {

        if ( dir == Direction.Right )
            slider.value = Mathf.Min( slider.value + Time.deltaTime*slider.maxValue, slider.maxValue );
        else
            slider.value = Mathf.Max( slider.value - Time.deltaTime*slider.maxValue, slider.minValue );
        seedPhy.force = new Vector2( slider.value, 0 );

        if ( slider.value == slider.maxValue )
            dir = Direction.Left;
        else if ( slider.value == slider.minValue )
            dir = Direction.Right;

        if ( throwTime != -1f && Time.time > (throwTime+2f))
        {
            isFly = false;
            if ( FunnyGamePet.CoinNum >= 8 && FunnyGamePet.SeedNum == 0 && !RestGame.isSuccess )
            {
                Img_Success.gameObject.SetActive( true );
                Img_Success.sprite = Success;
                Root_Game.SetActive( false );
                RestGame.isSuccess = true;
                PlayerPrefs.SetInt( GameInit.COINKEY, PlayerPrefs.GetInt( GameInit.COINKEY ) + FunnyGamePet.CoinNum );
				audioGame.ASVic.Play ();
            }
            else if (FunnyGamePet.SeedNum == 0 && FunnyGamePet.CoinNum < 8 )
            {
                Img_Success.gameObject.SetActive( true );
                Img_Success.sprite = Fail;
                Root_Game.SetActive( false );
                RestGame.isSuccess = false;
				audioGame.ASFail.Play ();
            }
            else if ( FunnyGamePet.CoinNum >= 8 && bonusTime == 0f && RestGame.isSuccess )
            {
                Img_Success.gameObject.SetActive( true );
                Img_Success.sprite = Success;
                Root_Game.SetActive( false );
                RestGame.isSuccess = false;
				audioGame.ASVic.Play ();
            }

            rigSeed.constraints = RigidbodyConstraints2D.FreezeAll;
            rigSeed.transform.localPosition = new Vector3( 0, -24.48f, 0 );
            rigSeed.transform.localRotation = Quaternion.identity;
        }
    }

    public void OnClick_Throw()
    {
        if ( isFly )
            return;

        if ( isBonusShow )
            return;

        isFly = true;
        rigSeed.constraints = RigidbodyConstraints2D.None;
        throwTime = Time.time;

        if ( !RestGame.isSuccess )
        {
            FunnyGamePet.SeedNum--;
            FunnyGamePet.SeedNum = Mathf.Max( 0, FunnyGamePet.SeedNum );
        }

        Text_SeedNum.text = string.Format( "{0}/10", FunnyGamePet.SeedNum );
    }

}
